from django.contrib.auth.models import User

from django.test import TestCase

class test_usuario(TestCase):
	def setUp(self):
		User.objects.create_superuser('nati013', 'nati@gmail.com', 'adminadmin')
	
	def test_registro_valido(self):
		print('Inicio de test registro_valido')
		url='/registrate/'
		registro={'username': 'Lilian',
				  'email': 'lilian@gmail.com',
				  'first_name': 'Lilian',
				  'last_name': 'Aguero',
				  'password1': 'adminadmin',
				  'password2': 'adminadmin',
		}
		print("Post to: " + url)
		self.client.login(username='Lilian',password1='adminadmin')
		respuesta=self.client.post(url,data=registro)
		if respuesta.status_code == 302:
			print("Registro cargado")
		print(respuesta.status_code)
		self.assertTrue(respuesta.status_code == 302,msg='No se ha podido registrar un usuario')

	def test_registro_invalido(self):
		print('Inicio de test registro_invalido') # El usuario no deberia cargarse ya que tiene un email incorrecto
		url = '/registrate/'
		antes = User.objects.count()	# Para hacer la verificacion de usuarios antes y despues
		registro = {'username': 'ole',
					'email': 'lalala',
					'first_name': 'Mateo',
					'last_name': 'Lopez',
					'password1': 'adminadmin',
				  	'password2': 'adminadmin',
					}
		print("Post to: " + url)
		self.client.login(username='nati013', password1='adminadmin')
		respuesta = self.client.post(url, data=registro)
		despues = User.objects.count()
		if antes == despues:
			#print("Antes: "+str(antes)+" Despues: "+str(despues))
			print("Registro no cargado")
		print(respuesta.status_code)
		self.assertTrue(antes == despues, msg='Se ha podido registrar al usuario')