# Trabajo práctico gcc 2019

<h4>Natalia Trinidad</h4>

Desarrollo de app: https://gcc-natalia-staging.herokuapp.com/ https://gcc-natalia-prod.herokuapp.com/


El archivo .gitlab-ci.yml ejecuta las siguientes fases:
<ul>
    <div><li>before_script, realiza la instalación de los paquetes especificados en requirements.txt</li></div>
    <div><li>Checking, verifica que todo el proyecto este compilado correctamente</li></div>
    <div><li>Testing, ejecuta los test/pruebas unitarias creadas para el proyecto</li></div>
    <div><li>Developer_deploy, instala las librerias necesarias y se encarga de desplegar la app de desarrollo en Heroku</li></div>
    <div><li>Production_deploy, instala las librerias necesarias y se encarga de desplegar la app de producción en Heroku</li></div>
</ul>

Para ingresar al sistema puede utilizar el usuario por default es Usuario y la contraseña es adminadmin* o puede registrar un usuario nuevo.
